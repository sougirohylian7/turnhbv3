package co.edu.uniajc.turnhb.controller;

import co.edu.uniajc.turnhb.model.servicioModel;
import co.edu.uniajc.turnhb.service.ServicioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;
import java.util.*;
import io.swagger.annotations.*;

@RestController
@RequestMapping("/service")
@Api("Services")
public class ServicioController {
    private ServicioService servicioService;

    @Autowired
    public ServicioController(ServicioService servicioService){ this.servicioService = servicioService; }
    
    @PostMapping(path = "/save")
    @ApiOperation(value="Insert Service",response = servicioModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Algo ha fallado"),
            @ApiResponse(code = 500, message = "Error interno del servidor")
    })
    public servicioModel saveService(@RequestBody servicioModel servicioModel){
        return servicioService.save(servicioModel);
    }

    public servicioModel getById(int id) {
        return servicioService.getById(id);
    }
    @GetMapping(path = "/all")
    @ApiOperation(value = "Buscar todos los servicios", response = servicioModel.class)
    public List<servicioModel> findAll() {
        return servicioService.findAll();
    }

    public Optional<servicioModel> findById(Long aLong) {
        return servicioService.findById(aLong);
    }
    @DeleteMapping(path = "/delete")
    @ApiOperation(value = "Borrar Servicio por ID", response = servicioModel.class)
    public void deleteById(Long aLong) {
        servicioService.deleteById(aLong);
    }
    @PutMapping(path = "/update")
    @ApiOperation(value = "Update Servicio", response = servicioModel.class)
    public servicioModel update(servicioModel servicioModel) {
        return servicioService.update(servicioModel);
    }
}


