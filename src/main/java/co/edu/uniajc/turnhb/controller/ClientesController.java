package main.java.co.edu.uniajc.turnhb.controller;

import main.java.co.edu.uniajc.turnhb.model.clientesModel;
import main.java.co.edu.uniajc.turnhb.service.ClientesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/clients")
@Api("Clients")
public class ClientesController {
    private ClientesService clientesService;

    @Autowired
    public ClientesController(ClientesService clientesService){
        this.clientesService = clientesService;
    }
    @PostMapping(path = "/save")
    @ApiOperation(value="Insert Client",response = clientesModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Algo ha fallado"),
            @ApiResponse(code = 500, message = "Error interno")
    })
    public clientesModel saveClient(@RequestBody clientesModel clientesModel){
        return clientesService.save(clientesModel);
    }

    public clientesModel getById(Long id) {
        return clientesService.getById(id);
    }
    @GetMapping(path = "/all")
    @ApiOperation(value = "Buscar todos los clientes", response = clientesModel.class)
    public List<clientesModel> findAll() {
        return clientesService.findAll();
    }

    public Optional<clientesModel> findById(Long aLong) {
        return clientesService.findById(aLong);
    }
    @DeleteMapping(path = "/delete")
    @ApiOperation(value = "Borrar Cliente por ID", response = clientesModel.class)
    public void deleteById(Long aLong) {
        clientesService.deleteById(aLong);
    }
    @PutMapping(path = "/update")
    @ApiOperation(value = "Update Client", response = clientesModel.class)
    public clientesModel update(clientesModel clientesModel) {
        return clientesService.update(clientesModel);
    }
}
