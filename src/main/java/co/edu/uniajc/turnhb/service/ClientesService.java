package main.java.co.edu.uniajc.turnhb.service;

import main.java.co.edu.uniajc.turnhb.model.clientesModel;
import main.java.co.edu.uniajc.turnhb.repository.ClientesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClientesService {
    private final ClientesRepository clientesRepository;
    @Autowired
    public ClientesService(ClientesRepository clientesRepository) {
        this.clientesRepository = clientesRepository;
    }

    public clientesModel getById(Long id) {
        return clientesRepository.getById(id);
    }

    @Query(nativeQuery = true, value = "SELECT" +
            "id" + ",celular" + ",nombre" + ",apellido" + ",estado" +
            "FROM cliente" + " WHERE estado=: estado")
    public List<clientesModel> findEstado(Integer estado) {
        return clientesRepository.findEstado(estado);
    }

    public List<clientesModel> findAll() {
        return clientesRepository.findAll();
    }

    public List<clientesModel> findAll(Sort sort) {
        return clientesRepository.findAll(sort);
    }

    public <S extends clientesModel> S save(S entity) {
        return clientesRepository.save(entity);
    }

    public Optional<clientesModel> findById(Long aLong) {
        return clientesRepository.findById(aLong);
    }

    public boolean existsById(Long aLong) {
        return clientesRepository.existsById(aLong);
    }

    public long count() {
        return clientesRepository.count();
    }

    public void deleteById(Long aLong) {
        clientesRepository.deleteById(aLong);
    }
    public clientesModel update(clientesModel clientesModel){
        if(existsById(clientesModel.getId()) == true)
        {
            clientesRepository.saveAndFlush(clientesModel);
        }
        return clientesModel;
    }
}
