package co.edu.uniajc.turnhb.model;

//import lombok.Data;


import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

@Entity
@Table(name = "Turno")
public class turnosModel implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	
    @Column(name="idTurno")
    private long idTurno;
    
	@Column(name="fechaTurno")
    private Date fechaTurno;
	
	@Column(name="horaTurno")
    private Time horaTurno;
	
	@Column(name="idServicio")
    private long idServicio;
    
    @Column(name="tipoServicio")
    private String tipoServicio;
    
    @Column(name="idEmpleado")
    private long idEmpleado;
    
    @Column(name="celularEmpleado")
    private long celularEmpleado;
    
    @Column(name="nombreEmpleado")
    private String nombreEmpleado;
    
    @Column(name="apellidoEmpleado")
    private String apellidoEmpleado;
    
    @Column(name="idCliente")
    private long idCliente;
    
    @Column(name="celularCliente")
    private long celularCliente;
    
    @Column(name="nombreCliente")
    private String nombreCliente;
    
    @Column(name="apellidoCliente")
    private String apellidoCliente;
    
    @Column(name="comentarioEmpleado")
    private String comentarioEmpleado;
    
    @Column(name="comentarioCliente")
    private String comentarioCliente;
    
    @Column(name="idEstadoTurno")
    private int idEstadoTurno;
    
    @Column(name="estadoTurno")
    private String estadoTurno;
    
    
    public turnosModel() {
    	//Constructor
    }
    
    public turnosModel(long idTurno, Date fechaTurno, Time horaTurno, long idServicio, String tipoServicio, long idEmpleado, long celularEmpleado,
    		String nombreEmpleado, String apellidoEmpleado, long idCliente, long celularCliente, String nombreCliente, String apellidoCliente, 
    		String comentarioEmpleado, String comentarioCliente, int idEstadoTurno, String estadoTurno) {
        this.idTurno = idTurno;
        this.fechaTurno = fechaTurno;
        this.horaTurno = horaTurno;
        this.idServicio = idServicio;
        this.tipoServicio = tipoServicio;
        this.idEmpleado = idEmpleado;
        this.celularEmpleado = celularEmpleado;
        this.nombreEmpleado = nombreEmpleado;
        this.apellidoEmpleado = apellidoEmpleado;
        this.idCliente = idCliente;
        this.celularCliente = celularCliente;
        this.nombreCliente = nombreCliente;
        this.apellidoCliente = apellidoCliente;
        this.comentarioEmpleado = comentarioEmpleado;
        this.comentarioCliente = comentarioCliente;
        this.idEstadoTurno = idEstadoTurno;
        this.estadoTurno = estadoTurno;
    }    
    
    public long getIdTurno() { return idTurno; }
    public void setIdTurno(int idTurno) { this.idTurno = idTurno; }

	public Date getFecha_Turno() { return fechaTurno; }
	public void setFecha_Turno(Date fechaTurno) { this.fechaTurno = fechaTurno; }

	public Time getHora() {	return horaTurno; }
	public void setHora(Time horaTurno) { this.horaTurno = horaTurno; }

	public long getIdServicio() { return idServicio; }
	public void setIdServicio(long idServicio) { this.idServicio = idServicio; }

	public String getTipoServicio() { return tipoServicio; }
	public void setTipoServicio(String tipoServicio) { this.tipoServicio = tipoServicio; }

	public long getIdEmpleado() { return idEmpleado; }
	public void setIdEmpleado(long idEmpleado) { this.idEmpleado = idEmpleado; }
	
	public long getCelularEmpleado() { return celularEmpleado; }
	public void setCelularEmpleado(long celularEmpleado) { this.celularEmpleado = celularEmpleado; }

	public String getNombreEmpleado() { return nombreEmpleado; }
	public void setNombreEmpleado(String nombreEmpleado) { this.nombreEmpleado = nombreEmpleado; }

	public String getApellidoEmpleado() { return apellidoEmpleado; }
	public void setApellidoEmpleado(String apellidoEmpleado) { this.apellidoEmpleado = apellidoEmpleado; }

	public long getIdCliente() { return idCliente; }
	public void setIdCliente(long idCliente) { this.idCliente = idCliente; }
	
	public long getCelularCliente() { return celularCliente; }
	public void setCelularCliente(long celularCliente) { this.celularCliente = celularCliente; }

	public String getNombreCliente() { return nombreCliente; }
	public void setNombreCliente(String nombreCliente) { this.nombreCliente = nombreCliente; }

	public String getApellidoCliente() { return apellidoCliente; }
	public void setApellidoCliente(String apellidoCliente) { this.apellidoCliente = apellidoCliente; }

	public String getComentarioEmpleado() { return comentarioEmpleado;}
	public void setComentarioEmpleado(String comentarioEmpleado) { this.comentarioEmpleado = comentarioEmpleado; }

	public String getComentarioCliente() { return comentarioCliente; }
	public void setComentarioCliente(String comentarioCliente) { this.comentarioCliente = comentarioCliente; }

	public int getIdEstadoTurno() { return idEstadoTurno; }
	public void setIdEstadoTurno(int idEstadoTurno) { this.idEstadoTurno = idEstadoTurno; }

	public String getEstadoTurno() { return estadoTurno; }
	public void setEstadoTurno(String estadoTurno) { this.estadoTurno = estadoTurno; }

}
