/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.java.co.edu.uniajc.turnhb.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author lenovo
 */
@Data
public class EmpleadosModel implements Serializable{

    private @Getter @Setter long id;
    private @Getter @Setter long celular;
    private @Getter @Setter String nombre;
    private @Getter @Setter String apellido;
    private @Getter @Setter Boolean estado;
}
