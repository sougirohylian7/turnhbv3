/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.java.co.edu.uniajc.turnhb.repository;

import main.java.co.edu.uniajc.turnhb.model.clientesModel;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author JhonSHylian
 */
@Repository
public interface ClientesRepository extends JpaRepository<clientesModel, Long>{
    List<clientesModel> findAllByNamesContains(String name);
    clientesModel getById(Long id);
    @Query(nativeQuery = true, value="SELECT"+
    "id"+",celular"+",nombre"+",apellido"+",estado"+
    "FROM cliente"+" WHERE estado=: estado")
    List<clientesModel> findEstado(@Param(value = "estado") Integer estado);
}